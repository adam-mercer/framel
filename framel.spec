%define upstream_version v8r41p3
%define version 8.41.3

Name: framel
Version: %{version}
Release: 1%{?dist}
Summary: LIGO/VIRGO frame library
License: LGPLv3+
Source: https://git.ligo.org/virgo/virgoapp/Fr/-/archive/%{upstream_version}/Fr-%{upstream_version}.tar.gz
URL: http://lappweb.in2p3.fr/virgo/FrameL
Packager: Duncan Macleod <duncan.macleod@ligo.org>
Prefix: %{_prefix}

# -- build requires ---------

# spec parsing
BuildRequires: python-rpm-macros
BuildRequires: python3-rpm-macros

# C
%if 0%{?rhel} == 0 || 0%{?rhel} >= 8
BuildRequires: cmake >= 3.12.0
%else
BuildRequires: cmake3 >= 3.12.0
%endif
BuildRequires: gcc
BuildRequires: glibc
BuildRequires: make

# python3
BuildRequires: python%{python3_pkgversion}-devel
BuildRequires: python%{python3_pkgversion}-numpy
BuildRequires: python%{python3_pkgversion}-pip

# -- package definitions ----

#%%package -n framel
Requires: lib%{name} = %{version}-%{release}
Provides: libframe-utils = %{version}-%{release}
Obsoletes: libframe-utils < 8.38.1-1
%description
A Common Data Frame Format for Interferometric Gravitational Wave Detector
has been developed by VIRGO and LIGO.  The Frame Library is a software
dedicated to the frame manipulation including file input/output.

This package contains some utilities for manipulating frame files.


%package -n lib%{name}
Summary: Shared object libraries for FrameL
%description -n lib%{name}
A Common Data Frame Format for Interferometric Gravitational Wave Detector
has been developed by VIRGO and LIGO.  The Frame Library is a software
dedicated to the frame manipulation including file input/output.

This package contains the shared-object library needed to run FrameL
applications.


%package -n lib%{name}-devel
Summary: Files and documentation needed for compiling FrameL programs
Requires: lib%{name} = %{version}-%{release}
Obsoletes: libframe-devel < 8.38.1-1
%description -n lib%{name}-devel
A Common Data Frame Format for Interferometric Gravitational Wave Detector
has been developed by VIRGO and LIGO.  The Frame Library is a software
dedicated to the frame manipulation including file input/output.

This package contains the files needed for building FrameL programs, as
well as the documentation for the library.


%package matlab
Summary: Matlab bindings for FrameL
Requires: lib%{name} = %{version}-%{release}
%description matlab
A Common Data Frame Format for Interferometric Gravitational Wave Detector
has been developed by VIRGO and LIGO.  The Frame Library is a software
dedicated to the frame manipulation including file input/output.

This package contains Matlab bindings for manipulating frame files from
within Matlab.


%package -n python%{python3_pkgversion}-%{name}
Summary: Python %{python3_version} bindings for FrameL
Requires: lib%{name} = %{version}-%{release}
Requires: python%{python3_pkgversion}
Requires: python%{python3_pkgversion}-numpy
%description -n python%{python3_pkgversion}-%{name}
A Common Data Frame Format for Interferometric Gravitational Wave Detector
has been developed by VIRGO and LIGO.  The Frame Library is a software
dedicated to the frame manipulation including file input/output.

This package contains Python %{python3_version} bindings for manipulating
frame files from within Python.


# -- build stages -----------

%prep
%setup -q -c -n %{name}-%{version}

%build
%cmake3 \
  -DCMAKE_BUILD_TYPE=Release \
  -DENABLE_C=yes \
  -DENABLE_MATLAB=yes \
  -DENABLE_PYTHON=yes \
  -DPython3_EXECUTABLE=%{__python3} \
  Fr-%{upstream_version}
%make_build

%install
%make_install

%check
export LD_LIBRARY_PATH="%{buildroot}%{_libdir}:${LD_LIBRARY_PATH}"
export PATH="%{buildroot}%{_bindir}:${PATH}"
export PKG_CONFIG_PATH="%{buildroot}%{_libdir}/pkgconfig:${PKG_CONFIG_PATH}"

# test executables
TEST_GWF="Fr-%{upstream_version}/data/test.gwf"
FrChannels ${TEST_GWF}
FrCheck -i ${TEST_GWF}
FrCopy -i ${TEST_GWF} -o copy.gwf
FrDiff -i1 ${TEST_GWF} -i2 copy.gwf -t D1 -d 1
FrDump -i ${TEST_GWF} -t D1
FrCopy -i ${TEST_GWF} -o copy2.gwf -f 0 -l 1 -r 10
FrTrend -s 1 -p trend- -d 5 -c D1 -f 925484670 -l 925484680 copy2.gwf

# run pkg-config to check metadata
test $(pkg-config --print-errors --modversion framel) == "%{version}"

# test python module
export PYTHONPATH="%{buildroot}%{python3_sitearch}:${PYTHONPATH}"
%{__python3} -c \
"from pathlib import Path;
from numpy.testing import assert_array_equal;
import framel;
indata = framel.frgetvect1d('${TEST_GWF}', 'D1');
framel.frputvect('test2.gwf', [{'name': 'A1:TEST', 'data': indata[0], 'start': indata[1], 'dx': indata[3], 'x_unit': indata[4], 'y_unit': indata[5]}]);
indata2 = framel.frgetvect1d('test2.gwf', 'A1:TEST');
assert_array_equal(indata[0], indata2[0], 'read-write data mismatch');
assert indata[1:] == indata2[1:], 'read-write metadata mismatch';
"
# check that pip knows about the python module
%{__python3} -m pip install --no-deps --index-url nothing %{name}

%post -n lib%{name} -p /sbin/ldconfig

%postun -n lib%{name} -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

# -- files ------------------

%files
%license Fr-%{upstream_version}/LICENSE
%{_bindir}/*
%{_docdir}/*

%files -n lib%{name}
%license Fr-%{upstream_version}/LICENSE
%{_libdir}/*.so.*

%files -n lib%{name}-devel
%license Fr-%{upstream_version}/LICENSE
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%files matlab
%license Fr-%{upstream_version}/LICENSE
%{_datadir}/framel/src/matlab/*

%files -n python%{python3_pkgversion}-%{name}
%license Fr-%{upstream_version}/LICENSE
%{python3_sitearch}/

# -- changelog --------------

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Wed Aug 18 2021 Duncan Macleod <duncan.macleod@ligo.org> 8.41.3-1
- Update to 8.41.3
- Remove python metadata patch
- Build python3-framel on EL8

* Mon Feb 8 2021 Duncan Macleod <duncan.macleod@ligo.org> 8.41.1-1
- Update to 8.41.1
- Only build python3-framel on EL7

* Mon Jun 15 2020 Duncan Macleod <duncan.macleod@ligo.org> 8.40.1-1
- Update to 8.40.1
- Improve tests for python module

* Tue May 12 2020 Duncan Macleod <duncan.macleod@ligo.org> 8.39.2-1
- Update to 8.39.2
- Rename packages for new SO name

* Wed May 6 2020 Duncan Macleod <duncan.macleod@ligo.org> 8.38.3-1
- Update to 8.38.3
- Rework packaging for cmake
- Introduce python bindings
- Separate executables and libraries

* Tue Apr 12 2016 Adam Mercer <adam.mercer@ligo.org> 8.30-1
- Update packaging for O2
